import { RouteConfig } from 'vue-router';

const routes: RouteConfig[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      // { path: '', component: () => import('pages/Index.vue')},
      { path: '', component: () => import('pages/Coaching.vue')},
      { path: 'payment', component: () => import('pages/Payment.vue')},
      { path: 'payment/thanks', component: () => import('pages/Payment/Thanks.vue')},
      { path: 'posts', component: () => import('pages/Posts.vue')}
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
];

export default routes;
